CREATE TABLE customers (
  customer_id INTEGER NOT NULL,
  last_name VARCHAR(30),
  first_name VARCHAR(30),
  street_address VARCHAR(40),
  city VARCHAR(30),
  state_province_code VARCHAR(2),
  postal_code VARCHAR(9),
  cust_geo_location GEOMETRY,
  CONSTRAINT customer_id_pk PRIMARY KEY (customer_id));
