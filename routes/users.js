var express = require('express');
var router = express.Router();
var app = express();
var db = app.get('db');

var userAttributes = [
  {label: 'User Name', name: 'usename'},
  {label: 'System Id', name: 'usesysid'}
]
/* GET users listing. */
router.get('/', function(req, res, next) {
  console.log("Found Route");
  req.app.get('db').run('select usename, usesysid from pg_user',function(err,result) {
    if(err){
      console.log(err);
      req.sendStatus(500);
    }else{
      console.log("Query Processed: "+JSON.stringify(result));
      res.render('users',{
        tab: 'users',
        title: 'DB Users',
        attributes: userAttributes,
        results:result
      });
    }
  });
});
module.exports = router;
